﻿using BenchmarkDotNet.Attributes;

namespace OTUS_Homework_7_Antipova_tasks_read_files.Helpers;

public class Benchmarks
{
    /*
    |                           Method |     Mean |     Error |    StdDev |
    |--------------------------------- |---------:|----------:|----------:|
    | CountSpacesInResourceFolderFiles | 1.427 ms | 0.0277 ms | 0.0340 ms |
     */
    [Benchmark]
    public async Task CountSpacesInResourceFolderFiles()
    {
        var result = await DirectoryProcessor.CountSpacesInResourceFolderFiles();
    }
}
