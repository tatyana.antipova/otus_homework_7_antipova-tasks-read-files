﻿using System.Diagnostics;

namespace OTUS_Homework_7_Antipova_tasks_read_files;

internal class Program
{
    static async Task Main(string[] args)
    {
        //BenchmarkRunner.Run<Benchmarks>();

        var sw = new Stopwatch();
        sw.Start();
        Console.WriteLine("Подсчет пробелов в локальных файлах: ");
        var result = await DirectoryProcessor.CountSpacesInResourceFolderFiles();

        foreach (var r in result)
            Console.WriteLine(r);

        sw.Stop();
        Console.WriteLine($"Операция выполнена за {sw.ElapsedMilliseconds} мс");

        while (true)
        {
            Console.WriteLine("Введите адрес папки для подсчета пробелов: ");
            var customPath = Console.ReadLine();

            sw.Reset();
            sw.Start();
            result = await DirectoryProcessor.CountSpacesInDirecoryFiles(customPath);
            sw.Stop();

            foreach (var r in result)
                Console.WriteLine(r);

            Console.WriteLine($"Операция выполнена за {sw.ElapsedMilliseconds} мс");
        }
    }
}