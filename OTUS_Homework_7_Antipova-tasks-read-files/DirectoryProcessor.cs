﻿using System.Reflection;

namespace OTUS_Homework_7_Antipova_tasks_read_files;

public static class DirectoryProcessor
{
    public static async Task<IEnumerable<string>> CountSpacesInDirecoryFiles(string directoryPath)
    {
        if (!Directory.Exists(directoryPath))
            throw new ArgumentException("Директория по указанному адресу не существует.", nameof(directoryPath));

        var files = Directory.GetFiles(directoryPath);
        if (files.Length == 0)
            throw new ArgumentException("В директории не найдено файлов.", nameof(directoryPath));

        var tasks = files.Select(async file =>
        {
            var result = await File.ReadAllTextAsync(file);
            var spaceCount = result.Count(ch => ch == ' ');
            return $"Файл {file.Split('\\').Last()} содержит {spaceCount} пробелов";
        });
        await Task.WhenAll(tasks);

        return tasks.Select(x => x.Result);
    }

    public static async Task<IEnumerable<string>> CountSpacesInResourceFolderFiles()
    {
        return await CountSpacesInDirecoryFiles(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Resources"));
    }
}


